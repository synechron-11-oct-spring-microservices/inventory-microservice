package com.classpath.inventorymicroservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/inventory")
@Slf4j
@RequiredArgsConstructor
public class InventoryController {

    private static int counter = 1000;
    private final Environment environment;

    @PostMapping
    public Integer updateInventory(){
        log.info("Came inside the update inventory method of inventory service :: {}", this.environment.getProperty("server.port"));
        return --counter;
    }

    @GetMapping
    public Integer counter(){
        return counter;
    }
}