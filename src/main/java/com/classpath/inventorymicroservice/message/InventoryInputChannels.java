package com.classpath.inventorymicroservice.message;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InventoryInputChannels {

    @Input("orderInputChannels")
    SubscribableChannel orderInputChannel();

}