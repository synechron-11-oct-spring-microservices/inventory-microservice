package com.classpath.inventorymicroservice.message;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface InventoryOutputChannels {


    @Output("inventoryOutputOrdersChannel")
    MessageChannel outputOrdersTopicChannel();

    @Output("invnenttoryOutputPaymentChannel")
    MessageChannel outputOrdersPaymentTopicChannel();


}