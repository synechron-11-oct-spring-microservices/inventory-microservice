package com.classpath.inventorymicroservice;

import com.classpath.inventorymicroservice.message.InventoryInputChannels;
import com.classpath.inventorymicroservice.message.InventoryOutputChannels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@EnableDiscoveryClient
@EnableBinding({InventoryInputChannels.class, InventoryOutputChannels.class})
public class InventoryMicroserviceApplication {
    public static void main(String[] args) {
        SpringApplication.run(InventoryMicroserviceApplication.class, args);
    }
}
