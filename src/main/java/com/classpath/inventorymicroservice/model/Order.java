package com.classpath.inventorymicroservice.model;
import lombok.*;
import java.time.LocalDate;

@NoArgsConstructor
@Data
@AllArgsConstructor
@EqualsAndHashCode(exclude = "lineItems")
@ToString
public class Order {
    private long id;
    private double price;
    private LocalDate date;
    private String customerEmail;
}