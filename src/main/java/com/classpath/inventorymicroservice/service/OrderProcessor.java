package com.classpath.inventorymicroservice.service;

import com.classpath.inventorymicroservice.message.InventoryInputChannels;
import com.classpath.inventorymicroservice.message.InventoryOutputChannels;
import com.classpath.inventorymicroservice.model.Order;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderProcessor {

    private final InventoryOutputChannels outputChannels;

    @StreamListener("orderInputChannels")
    public void processOrder(Order order){
        System.out.println("Inside the process order method :: " + order);
      log.info("Processing the order , :: {} ", order);
      log.info("Pushing the message to processed_order channel");
      this.outputChannels.outputOrdersPaymentTopicChannel().send(MessageBuilder.withPayload(order).build());
      this.outputChannels.outputOrdersTopicChannel().send(MessageBuilder.withPayload(order).build());
    }
}